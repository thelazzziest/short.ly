import { Application } from 'express';
import shortlyRouter from './index/router';
export default function routes(app: Application): void {
  app.use('/', shortlyRouter);
}
