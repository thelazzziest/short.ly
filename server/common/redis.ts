import redis from "redis";
import l from "./logger";

const redisClient = redis.createClient(
    parseInt(process.env.REDIS_PORT),
    process.env.REDIS_HOST
);
redisClient.on('connect', status => {
    l.info(status);
    l.info('Redis client has been connected to the server');
});
redisClient.on('error', error => {
    l.error(error);
    l.error('Failed to connect to redis server.');
});

export default redisClient;
