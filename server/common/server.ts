import express, { Application } from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import http from 'http';
import os from 'os';
import cookieParser from 'cookie-parser';
import cookieSession from 'cookie-session';
import flash from 'express-flash';
import l from './logger';
import csrfMiddleware from './csrf';

const app = express();
const exit = process.exit;

export default class ExpressServer {
  constructor() {
    const root = path.normalize(__dirname + '/../..');
    app.set('appPath', root + 'client');
    // Configure Express to use EJS
    app.set('view engine', 'ejs');
    app.set('views', path.join(root, 'server/views'));
    // Configure body parser
    app.use(
      bodyParser.json({
        limit: process.env.REQUEST_LIMIT || '100kb',
      })
    );
    app.use(
      bodyParser.urlencoded({
        extended: true,
        limit: process.env.REQUEST_LIMIT || '100kb',
      })
    );
    app.use(bodyParser.text({ limit: process.env.REQUEST_LIMIT || '100kb' }));
    // Connect flash messages
    app.use(flash());
    // Configure express
    app.use(cookieParser(process.env.SESSION_SECRET));
    app.use(csrfMiddleware);
    app.use(
      cookieSession({
        name: 'shly',
        keys: [process.env.SESSION_SECRET],
        maxAge: 60 * 60 * 1000, // 1 hour
      })
    );
    app.use(express.static(`${root}/public`));
  }

  router(routes: (app: Application) => void): ExpressServer {
    routes(app);
    return this;
  }

  listen(port: number): Application {
    const welcome = (p: number) => () =>
      l.info(
        `up and running in ${process.env.NODE_ENV ||
          'development'} @: ${os.hostname()} on port: ${p}}`
      );
    try {
      http.createServer(app).listen(port, welcome(port));
    } catch (e) {
      l.error(e);
      exit(1);
    }

    return app;
  }
}
