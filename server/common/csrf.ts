import csrf from 'csurf';

const csrfMiddlware = csrf({ cookie: true });
export default csrfMiddlware