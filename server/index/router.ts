import express from 'express';
import controller from './controllers/controller';
import errorHandler from './middlewares/error.handler';

export default express
  .Router()
  .use(errorHandler)
  .get('/', controller.index)
  .post('/create', controller.create)
  .get('/link/:link', controller.link);
