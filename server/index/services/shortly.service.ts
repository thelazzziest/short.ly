import shortid from 'shortid';
import L from "../../common/logger";

interface Alias {
  site: string;
  link: string;
}

export class ShortlyService {
  /**
   * Create a short link for a site url
   * @param site string
   */
  createLink(site: string): Promise<Alias> {
    const entry: Alias = { link: shortid.generate(), site: site };
    return Promise.resolve(entry);
  }

  /**
   * Deserialize object
   * @param data
   */
  toObj(data: string): Promise<Alias> {
    return Promise.resolve(JSON.parse(data));
  }

  /**
   * Serializer object
   * @param data: Alias
   */
  toString(data: Alias): Promise<string> {
    return Promise.resolve(JSON.stringify(data));
  }
}

export default new ShortlyService();
