import {Request, Response} from 'express';
import redisClient from '../../common/redis';
import ShortlyService from '../services/shortly.service';

export class Controller {
  index(req: Request, res: Response): void {
    redisClient.hget('data', `users:${req.cookies.sesid}`, (err, r) => {
      ShortlyService.toObj(r).then(data => {
        const ctx = {
          createLink: '/create',
          token: req.csrfToken(),
          site: (data && data.site) || '',
          link: (data && req.protocol + "://" + req.get('host') + '/link/' + data.link) || '',
        };
        console.log("Index: ", res.locals.messages);
        res.render('index', ctx);
      });
    });
  }

  link(req: Request, res: Response): void {
    redisClient.hget('data', `users:${req.cookies.sesid}`, (err, r) => {
      ShortlyService.toObj(r).then(data => {
        if (data.link != req.params.link) {
          console.log('Link:error', data.link, req.params.link);
          req.flash('error', "This link doesn't belong to you.");
          res.redirect('/');
        } else {
          res.redirect(data.site);
        }
      });
    });
  }

  create(req: Request, res: Response): void {
    ShortlyService.createLink(
      req.body.site
    ).then(data => {
      ShortlyService.toString(data).then(data => {
        redisClient.hset(
          'data',
          `users:${req.cookies.sesid}`,
          data,
          (err, r) => {
            req.flash('success', 'Link has been created!');
            res.redirect('/');
          }
        );
      });
    });
  }
}

export default new Controller();
