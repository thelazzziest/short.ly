import 'mocha';
import { expect } from 'chai';
import request from 'supertest';
import Server from '../server';

describe('Index', () => {
  it('should get all shortly', () =>
    request(Server)
      .get("/")
      .expect('Content-Type', /html/));

  it('should add a new link for url', () =>
    request(Server)
      .post('/create')
      .send({ site: "https://yandex.ru" })
      .expect("Content-Type", /html/));
});
