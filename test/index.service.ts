import 'mocha';
import { expect } from 'chai';
import { ShortlyService } from "../server/index/services/shortly.service";

describe("Index", () => {
  it("get datastructure for storing site data", () =>
    ShortlyService.createLink('https://yandex.ru').then( data => {
       data.should.have.property('link');
       data.should.have.property('site');
    }));
});
